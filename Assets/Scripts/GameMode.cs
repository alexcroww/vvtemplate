﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using System.Linq;

public class GameMode : MonoBehaviour
{
	static GameMode inst;

	public static GameMode Inst {
		get {
			if (!inst) {
				inst = Resources.FindObjectsOfTypeAll<GameMode>().FirstOrDefault();
			}

			return inst;
		}
	}

	void OnEnable()
	{
		if (SceneManager.GetActiveScene().buildIndex != 0)
			ManyScene();
		SetStart();
		rx();
	}

	void rx()
	{
		VButton.onClick.Where(x => x.nameObject == "back_gamemode").TakeUntilDisable(this)
			.Subscribe(_ => SceneManager.LoadSceneAsync("Main"));
	}

	void SetStart() { }

	void ManyScene()
	{
		Setting.Inst.Init();

		Observable.EveryUpdate().TakeUntilDisable(this).Subscribe(_ =>
		{
			if (Input.GetKeyDown(KeyCode.Escape)) {
				VSound.Click();
				SceneManager.LoadSceneAsync("Main");
			}
		});
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UnityEditor.AnimatedValues;
using UniRx;

[CustomEditor(typeof(VUIAnimator))]
[CanEditMultipleObjects]
public class VUIAnimatorEditor : Editor
{
	VUIAnimator script;

	Dictionary<string, ReorderableListProperty> reorderableLists;
	static CompositeDisposable disAnima = new CompositeDisposable();
	static CompositeDisposable dis = new CompositeDisposable();
	int lastState;
	float timeTemp = 0f;

	void OnEnable()
	{
		reorderableLists = new Dictionary<string, ReorderableListProperty>(10);

		script = (VUIAnimator) target;
		lastState = script.state.Value;

		if (script.image == null) {
			script.image = script.GetComponent<Image>();
			script.rectTrans = script.image.rectTransform;
		}

		script.state.Where(x => x >= 0).Subscribe(x =>
		{
			if (x == lastState || Application.isPlaying)
				return;
			var st = script.stateList[x];
			const float tik = 0.01f;

			disAnima.Clear();

			script.Animation(x);
//			Animation(x);

			Observable.Interval(TimeSpan.FromSeconds(tik), Scheduler.MainThreadIgnoreTimeScale)
				.Take((int) (st.time / tik) + 1).Subscribe(_ =>
				{
					script.posTween.fullPosition += tik;
					script.scaleTween.fullPosition += tik;
					script.colorTween.fullPosition += tik;
				}).AddTo(disAnima);

			lastState = x;
		}).AddTo(dis);
	}

	void OnDisable()
	{
		dis.Clear();
		disAnima.Clear();
	}

	void Animation(int x)
	{
		var eas = (script.stateList.Count > x ? script.stateList[x].ease : DOTween.defaultEaseType);
		var tim = (script.stateList.Count > x ? script.stateList[x].time : 0.5f);
		if (script.stateList.Count > x && script.stateList[x].pos != Vector2.one * -1)
			script.posTween = script.GetComponent<RectTransform>().DOAnchorPos(script.stateList[x].pos, tim)
				.SetEase(eas);
		if (script.stateList.Count > x && script.stateList[x].scale != Vector3.one * -1)
			script.scaleTween = script.GetComponent<RectTransform>().DOScale(script.stateList[x].scale, tim)
				.SetEase(eas);
		if (script.stateList.Count > x && script.stateList[x].color != new Color())
			script.colorTween = script.GetComponent<Image>().DOColor(script.stateList[x].color, tim).SetEase(eas);
	}

	ReorderableListProperty GetReorderableList(SerializedProperty property)
	{
		ReorderableListProperty ret = null;
		if (reorderableLists.TryGetValue(property.name, out ret)) {
			ret.Property = property;
			return ret;
		}

		ret = new ReorderableListProperty(property);
		reorderableLists.Add(property.name, ret);
		return ret;
	}

	#region Inner-class ReorderableListProperty

	private class ReorderableListProperty
	{
		public AnimBool IsExpanded { get; private set; }

		/// <summary>
		/// ref http://va.lent.in/unity-make-your-lists-functional-with-reorderablelist/
		/// </summary>
		public ReorderableList List { get; private set; }

		private SerializedProperty _property;

		public SerializedProperty Property {
			get { return _property; }
			set {
				_property = value;
				List.serializedProperty = _property;
			}
		}

		public ReorderableListProperty(SerializedProperty property)
		{
			IsExpanded = new AnimBool(property.isExpanded);
			IsExpanded.speed = 1f;
			_property = property;
			CreateList();
		}

		~ReorderableListProperty()
		{
			_property = null;
			List = null;
		}

		private void CreateList()
		{
			bool dragable = true, header = true, add = true, remove = true;
			List = new ReorderableList(Property.serializedObject, Property, dragable, header, add, remove);
			List.drawHeaderCallback += rect =>
				_property.isExpanded = EditorGUI.ToggleLeft(rect, _property.displayName, _property.isExpanded,
					EditorStyles.boldLabel);
			List.onCanRemoveCallback += (list) => List.count > 0;
			List.drawElementCallback += drawElement;
			List.elementHeightCallback +=
				(idx) => Mathf.Max(EditorGUIUtility.singleLineHeight,
					         EditorGUI.GetPropertyHeight(_property.GetArrayElementAtIndex(idx), GUIContent.none,
						         true)) + 4.0f;
		}

		private void drawElement(Rect rect, int index, bool active, bool focused)
		{
			if (_property.GetArrayElementAtIndex(index).propertyType == SerializedPropertyType.Generic) {
				EditorGUI.LabelField(rect, _property.GetArrayElementAtIndex(index).displayName);
			}

			//rect.height = 16;
			rect.height = EditorGUI.GetPropertyHeight(_property.GetArrayElementAtIndex(index), GUIContent.none, true);
			rect.y += 1;
			EditorGUI.PropertyField(rect, _property.GetArrayElementAtIndex(index), GUIContent.none, true);
			List.elementHeight = rect.height + 4.0f;
		}
	}

	#endregion

	protected void HandleProperty(SerializedProperty property)
	{
		//Debug.LogFormat("name: {0}, displayName: {1}, type: {2}, propertyType: {3}, path: {4}", property.name, property.displayName, property.type, property.propertyType, property.propertyPath);
		bool isdefaultScriptProperty = property.name.Equals("m_Script") && property.type.Equals("PPtr<MonoScript>") &&
		                               property.propertyType == SerializedPropertyType.ObjectReference &&
		                               property.propertyPath.Equals("m_Script");
		bool cachedGUIEnabled = GUI.enabled;
		if (isdefaultScriptProperty)
			GUI.enabled = false;
		//var attr = this.GetPropertyAttributes(property);
		if (property.isArray && property.propertyType != SerializedPropertyType.String)
			HandleArray(property);
		else
			EditorGUILayout.PropertyField(property, property.isExpanded);
		if (isdefaultScriptProperty)
			GUI.enabled = cachedGUIEnabled;
	}

	protected void HandleArray(SerializedProperty property)
	{
		var listData = GetReorderableList(property);
		listData.IsExpanded.target = property.isExpanded;
		if ((!listData.IsExpanded.value && !listData.IsExpanded.isAnimating) ||
		    (!listData.IsExpanded.value && listData.IsExpanded.isAnimating)) {
			EditorGUILayout.BeginHorizontal();
			property.isExpanded = EditorGUILayout.ToggleLeft(string.Format("{0}", property.displayName),
				property.isExpanded, EditorStyles.boldLabel);
			EditorGUILayout.LabelField(string.Format("Размер: {0}", property.arraySize));
			EditorGUILayout.EndHorizontal();
		}
		else {
			if (EditorGUILayout.BeginFadeGroup(listData.IsExpanded.faded))
				listData.List.DoLayoutList();
			EditorGUILayout.EndFadeGroup();
		}
	}

	protected object[] GetPropertyAttributes(SerializedProperty property)
	{
		return GetPropertyAttributes<PropertyAttribute>(property);
	}

	protected object[] GetPropertyAttributes<T>(SerializedProperty property) where T : System.Attribute
	{
		System.Reflection.BindingFlags bindingFlags = System.Reflection.BindingFlags.GetField
		                                              | System.Reflection.BindingFlags.GetProperty
		                                              | System.Reflection.BindingFlags.IgnoreCase
		                                              | System.Reflection.BindingFlags.Instance
		                                              | System.Reflection.BindingFlags.NonPublic
		                                              | System.Reflection.BindingFlags.Public;
		if (property.serializedObject.targetObject == null)
			return null;
		var targetType = property.serializedObject.targetObject.GetType();
		var field = targetType.GetField(property.name, bindingFlags);
		if (field != null)
			return field.GetCustomAttributes(typeof(T), true);
		return null;
	}

	public override void OnInspectorGUI()
	{
		script = (VUIAnimator) target;
//		DrawDefaultInspector();
//		reorderableList.DoLayoutList();
		Color cachedGuiColor = GUI.color;
		serializedObject.Update();

		var property = serializedObject.GetIterator();
		var next = property.NextVisible(true);
		if (next)
			do {
				GUI.color = cachedGuiColor;
				HandleProperty(property);
			} while (property.NextVisible(false));

		if (script != null && script.stateList != null && script.stateList.Count > 0)
			script.state.Value =
				EditorGUILayout.IntSlider("Состояние", script.state.Value, 0, script.stateList.Count - 1);

		serializedObject.ApplyModifiedProperties();
	}


	string tempPars = "0.3";

	void OnSceneGUI()
	{
		script = (VUIAnimator) target;
		var guiStyle = new GUIStyle();

		Handles.BeginGUI();

//		var v2 = Event.current.mousePosition;
		var v2 = Camera.current.WorldToScreenPoint(script.transform.position);

		GUILayout.BeginArea(new Rect(v2.x, Screen.height - v2.y - 36, 100, 50));

		var rect = EditorGUILayout.BeginVertical();
		GUI.color = Color.black;
		GUI.Box(rect, GUIContent.none);

		GUI.color = Color.white;
		GUI.backgroundColor = Color.green;
		if (GUILayout.Button(new GUIContent("+", "Добавить состояние"), EditorStyles.miniButton)) {
			SetState();
		}

		GUI.backgroundColor = Color.white;

		GUILayout.BeginHorizontal();
		guiStyle.fontStyle = FontStyle.Bold;
		GUILayout.Label(new GUIContent("Время", "Время анимации состояния"), guiStyle);

		tempPars = GUILayout.TextField(VLab.StringFloat(tempPars));
		if (float.TryParse(tempPars, out timeTemp))
			timeTemp.ToString();

		GUILayout.EndHorizontal();

		EditorGUILayout.EndVertical();

		GUILayout.EndArea();

		Handles.EndGUI();

		DrawState();
	}

	void SetState()
	{
		var rect = script.GetComponent<RectTransform>();
		var image = script.GetComponent<Image>();
		script.stateList.Add(new VUIAnimator.State(timeTemp, rect.anchoredPosition, rect.localScale, image.color,
			DOTween.defaultEaseType, script.gameObject.activeSelf, rect.position));
	}

	void DrawStateBad()
	{
		foreach (var item in script.stateList) {
			var posTemp = Camera.current.WorldToScreenPoint(item.posWorld);
			var rectTrans = script.GetComponent<RectTransform>();
			var magicK = (Camera.current.orthographicSize / Camera.main.orthographicSize) /
			             ((Screen.height - 36) / 1280f);

			Handles.DrawSolidRectangleWithOutline(new Rect(
					posTemp.x - rectTrans.rect.width / magicK * rectTrans.pivot.x,
					Screen.height - posTemp.y - 36 - rectTrans.rect.height / magicK * (1f - rectTrans.pivot.y),
					rectTrans.rect.width / magicK,
					rectTrans.rect.height / magicK),
				new Color(0f, 0.5f, 0f, 0.1f), Color.green);
		}
	}

	void DrawState()
	{
		if (script.stateList != null && script.stateList.Count > 0)
			foreach (var item in script.stateList) {
				var magicK = script.transform.root.transform.localScale.x;
				var rectTrans = script.GetComponent<RectTransform>();
				var rect = new Rect(item.posWorld.x - rectTrans.rect.width * magicK * rectTrans.pivot.x * item.scale.x,
					item.posWorld.y - rectTrans.rect.height * magicK * rectTrans.pivot.y * item.scale.y,
					rectTrans.rect.width * magicK * item.scale.x, rectTrans.rect.height * magicK * item.scale.y);

				Handles.DrawSolidRectangleWithOutline(rect,
					new Color(item.color.r, item.color.g, item.color.b, item.color.a * 0.05f),
					new Color(item.color.r, item.color.g, item.color.b, Mathf.Clamp(item.color.a, 0.1f, 0.5f)));
			}
	}
}
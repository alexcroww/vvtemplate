﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UniRx;

[CustomEditor(typeof(VStateAnimator))]
[CanEditMultipleObjects]
public class VStateAnimatorEditor : Editor
{
	VStateAnimator script;

	int _state = 0;

	void OnEnable()
	{
		script = (VStateAnimator) target;

		if (!Application.isPlaying) {
			script.stateValue.Where(x => script.stateList.Count > 1).TakeUntilDisable(script)
				.Subscribe(x => { script.Change(); });
		}
	}

	public override void OnInspectorGUI()
	{
		script = (VStateAnimator) target;

		DrawDefaultInspector();
		GUI.backgroundColor = new Color(0.8f, 1f, 0.8f, 1f);
		if (script.stateList.Count > 0)
			_state = EditorGUILayout.IntSlider("Состояние", _state, 0, script.stateList.Count);
		if (GUILayout.Button(new GUIContent("+", "Добавить данные состояния"))) {
			var stAnim = new VStateAnimator.State(script.transform.localPosition,
				script.transform.localRotation.eulerAngles,
				script.transform.localScale);
			if (script.stateList.Count - 1 < _state) {
				script.stateList.Add(stAnim);
			}
			else
				script.stateList[_state] = stAnim;

			_state++;
		}

		GUI.backgroundColor = Color.white;
	}
}

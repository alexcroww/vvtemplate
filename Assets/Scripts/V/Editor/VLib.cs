﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Configuration;

public class VLib : EditorWindow
{
	public GameObject[] objAList;
	public GameObject objA;
	public GameObject[] objBList;

	[MenuItem("Window/VLib")]
	static void Init()
	{
		var window = (VLib) EditorWindow.GetWindow(typeof(VLib));
		window.Show();
	}

	private Vector2 scrollPos;

	void OnGUI()
	{
		scrollPos = GUILayout.BeginScrollView(scrollPos, false, false);

		GUILayout.Label("Объект для клонирования", EditorStyles.boldLabel);
//		objA = (GameObject) EditorGUILayout.ObjectField(objA, typeof(GameObject));

		ScriptableObject target = this;
		SerializedObject so = new SerializedObject(target);

//		SerializedProperty objAProperty = so.FindProperty("objAList");
//		EditorGUILayout.PropertyField(objAProperty, true);
		SerializedProperty objAProperty = so.FindProperty("objA");
		EditorGUILayout.PropertyField(objAProperty, true);

		GUILayout.Label("Пути в которых этот объект появится", EditorStyles.boldLabel);
//		objB = (GameObject) EditorGUILayout.ObjectField(objB, typeof(GameObject));

		SerializedProperty objBProperty = so.FindProperty("objBList");
		EditorGUILayout.PropertyField(objBProperty, true);

		so.ApplyModifiedProperties();

		if (GUILayout.Button("Склонировать")) {
//			if (objA != null && objB != null)
//				objB.transform.position = new Vector3(objA.transform.position.x, objA.transform.position.y,
//					objB.transform.position.z);

//			for (int i = 0; i < objAList.Length; i++) {
//				if (objAList[i] != null && objBList.Length > i && objBList[i] != null)
//					objBList[i].transform.position = new Vector3(objAList[i].transform.position.x,
//						objAList[i].transform.position.y,
//						objBList[i].transform.position.z);
//			}
			for (int i = 0; i < objBList.Length; i++) {
				if (objA == null || objBList[i] == null)
					continue;
				var newObj = Instantiate(objA, objBList[i].transform);
				newObj.transform.localPosition = objA.transform.localPosition;
				newObj.transform.localScale = objA.transform.localScale;
				newObj.transform.SetSiblingIndex(objA.transform.GetSiblingIndex());
				newObj.name = objA.name;
			}
		}

		GUILayout.EndScrollView();
	}
}

public class VMenu : MonoBehaviour
{
	[MenuItem("VLib/Удалить PlayerPrefs")]
	static void DeletePrefs()
	{
		PlayerPrefs.DeleteAll();
	}

	//	[MenuItem("VLib/Создание текстур")]
//	static void MaterialRoutine()
//	{
//		int sizeX = 512;
//		int sizeY = 512;
//
//		Color c1 = new Color(1f, 0.2f, 0f, 1f);
//		Color c2 = new Color(0f, 1f, 0f, 0.5f);
//
//		Texture2D tex = new Texture2D(sizeX, sizeY);
//
//		for (int y = 0; y < sizeY; ++y) {
//			for (int x = 0; x < sizeX; ++x)
//				tex.SetPixel(x, y, new Color(0f, 0f, 0f, 0f));
//		}
//
//		for (int y = 1; y < sizeY; ++y) {
//			for (int x = 1; x < sizeX; ++x)
////				tex.SetPixel(x, y, (c1 * x / y + c2 * y / x) * Random.value);
//				tex.SetPixel(x, y, new Color(Mathf.Sin(x * x - y * y),
//					Mathf.Sin(x - y * y),
//					Mathf.Sin(y * y - y),
//					Mathf.Cos(x * x - y * y)));
//		}
//
//		byte[] pngData = tex.EncodeToPNG();
//		if (pngData != null)
//			File.WriteAllBytes("Assets/Art/vArt/vTexture.png", pngData);
//
//		DestroyImmediate(tex);
//		AssetDatabase.Refresh();
//	}
}
﻿using UnityEngine;
using System.Collections.Generic;
using UniRx;
using System.Linq;
using DG.Tweening;

public class VSound : MonoBehaviour
{
	public static string RESOURCES_SOUND_PATH = "sound";
	public static string TAP_CLIP_NAME = "click";
	public static string TAP_BACK_CLIP_NAME = "back";

	public int ID;
	static List<VAudioSource> clips = new List<VAudioSource>();

	public class VAudioSource
	{
		public int id;
		public string clipName;
		public AudioClip clip;
		public AudioSource source;
		public bool destroy;
		public bool isMusic;
		public GameObject obj;

		public VAudioSource(string clipName, AudioClip clip, AudioSource source, bool destroy, int id, bool isMusic,
			GameObject obj)
		{
			this.clipName = clipName;
			this.clip = clip;
			this.source = source;
			this.destroy = destroy;
			this.id = id;
			this.isMusic = isMusic;
			this.obj = obj;
		}
	}

	public static void Click()
	{
		Play(TAP_CLIP_NAME);
	}

	public static void ClickBack()
	{
		Play(TAP_BACK_CLIP_NAME);
	}
	
	static VSound.VAudioSource AddSound(string clipName, bool destroy)
	{
		var clip = Resources.Load<AudioClip>(RESOURCES_SOUND_PATH + "/" + clipName);
#if UNITY_EDITOR
		if (clip == null) {
			Debug.LogError(
				"Аудиозапись <color=#ffff00>" + RESOURCES_SOUND_PATH + "/" + clipName + "</color> не найдена");
			return null;
		}
#endif

		var go = new GameObject();
		var source = go.AddComponent<AudioSource>();
		var vs = go.AddComponent<VSound>();
		source.clip = clip;
		source.playOnAwake = false;
		go.name = clip.name;

		for (int i = 1; i <= clips.Count; i++) {
			if (clips.Any(x => x.id == i))
				continue;
			vs.ID = i;
			break;
		}

		var vAudioSource = new VAudioSource(clipName, clip, source, destroy, vs.ID, false, go);
		clips.Add(vAudioSource);

		return vAudioSource;
	}

	void Start()
	{
		VPreferences.soundEnabled.Subscribe(b =>
		{
			var vas = clips.FirstOrDefault(x => x.id == ID && !x.isMusic);
			if (vas != null) vas.source.mute = !b;
		}).AddTo(this);
		VPreferences.musicEnabled.Subscribe(b =>
		{
			var vas = clips.FirstOrDefault(x => x.id == ID && x.isMusic);
			if (vas != null) vas.source.mute = !b;
		}).AddTo(this);

		DontDestroyOnLoad(gameObject);
	}

	//	void OnDestroy()
	//	{
	//		clips.Remove(clips.FirstOrDefault(x => x.id == ID));
	//	}

	public static VAudioSource Play(string clipName, bool isLoop = false, bool isUnique = true, bool destroy = true)
	{
		var clip = clips.FirstOrDefault(x => x.clipName == clipName && (!x.source.isPlaying || isUnique));
		if (clip != null) {
			clip.source.Play();
			return clip;
		}
		else {
			var sound = AddSound(clipName, destroy);
#if UNITY_EDITOR
			if (sound == null) {
				Debug.LogError("Аудиозапись <color=#ffff00>" + clipName + "</color> не найдена!");
				return null;
			}
#endif
			sound.source.loop |= isLoop;
			sound.source.Play();
			return sound;
		}
	}

	public static bool IsPlaying(string clipName)
	{
		return clips.Any(x => x.clipName == clipName && x.source.isPlaying);
	}

	public static void Stop(string clipName, bool isAll = true)
	{
		if (isAll) {
			var list = clips.FindAll(x => x.clipName == clipName);
			foreach (var item in list)
				item.source.Stop();
		}
		else {
			var clip = clips.FirstOrDefault(x => x.clipName == clipName);
			if (clip != null)
				clip.source.Stop();
		}
	}

	public static void VolumeFade(string clipName, float time, float start, float end)
	{
		var clip = clips.FirstOrDefault(x => x.clipName == clipName);
		if (clip != null) {
			DOVirtual.Float(start, end, time, x => { clip.source.volume = x; }).SetEase(Ease.Linear);
		}
	}

	public static void Pause(string clipName, bool isAll = true)
	{
		if (isAll) {
			var list = clips.FindAll(x => x.clipName == clipName);
			foreach (var item in list)
				item.source.Pause();
		}
		else {
			var clip = clips.FirstOrDefault(x => x.clipName == clipName);
			if (clip != null)
				clip.source.Pause();
		}
	}

	public static void StopAllSounds()
	{
		foreach (var item in clips)
			if (item.clipName != TAP_CLIP_NAME)
				if (item.source != null)
					item.source.Stop();
	}

	public static void PauseAllSounds()
	{
		foreach (var item in clips)
			if (item.clipName != TAP_CLIP_NAME)
				if (item.source != null)
					item.source.Pause();
	}

	public static void DestroyAllSounds()
	{
		var list = clips.FindAll(x => x.destroy);
		for (int i = 0; i < list.Count; i++) {
			clips.Remove(list[i]);
			if (list[i] != null && list[i].source != null)
				Destroy(list[i].source.gameObject);
		}
	}

//	void DestroySound()
//	{
//		var vas = clips.FirstOrDefault(x => x.obj == gameObject);
//		if (vas != null) {
//			clips.Remove(vas);
//		}
//		Destroy(gameObject);
//	}

	public static VAudioSource FindVAudioSource(string clipName)
	{
		return clips.FirstOrDefault(x => x.clipName == clipName);
	}

	private void OnDestroy()
	{
		var vas = clips.FirstOrDefault(x => x.obj == gameObject);
		if (vas != null) {
			clips.Remove(vas);
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using DG.Tweening;
using System.Linq;
using DG.Tweening.Core;

public class VStateAnimator : MonoBehaviour
{
	public bool isScene = true;
	public string nameObj;
	public List<State> stateList;
	public FloatReactiveProperty stateValue;

	Tween mainTween;

	void OnEnable()
	{
		stateValue.TakeUntilDisable(this).Subscribe(x => Change());
		isScene = gameObject.scene.name != null;
	}

	void OnDisable()
	{
		Change();
	}

	public void Change()
	{
		var st = Mathf.FloorToInt(stateValue.Value);
		if (stateList.Count <= st + 1 || stateValue.Value < 0)
			return;
		var w1 = 1 - stateValue.Value + st;
		var w2 = stateValue.Value - st;
		transform.localPosition = stateList[st].pos * w1 + stateList[st + 1].pos * w2;
		transform.localRotation = Quaternion.Euler(stateList[st].rot * w1 + stateList[st + 1].rot * w2);
		transform.localScale = stateList[st].scale * w1 + stateList[st + 1].scale * w2;
	}

	public void TweenAnimator(float time, float start, float end, Ease easy, bool isCurrentPos)
	{
		if (mainTween != null)
			mainTween.Kill();
		mainTween = DOVirtual.Float(isCurrentPos ? stateValue.Value : start, end, time, x => {
			stateValue.Value = x;
		}).SetEase(easy);
	}

	public static void SetStateValue(string nameAnim, float valueAnim)
	{
		var list = Resources.FindObjectsOfTypeAll<VStateAnimator>().Where(x => x.isScene = true).ToList();
		var animatorNameList = list.FindAll(x => x.nameObj == nameAnim && x.gameObject.activeInHierarchy);

		foreach (var item in animatorNameList) {
			item.stateValue.Value = valueAnim;
		}
	}

	public static void PlayAnima(string nameAnim, float time, float start, float end, Ease easy = Ease.Linear, bool isCurrentPos = true)
	{
		var list = Resources.FindObjectsOfTypeAll<VStateAnimator>().Where(x => x.isScene = true).ToList();
		var animatorNameList = list.FindAll(x => x.nameObj == nameAnim && x.gameObject.activeInHierarchy);

		foreach (var item in animatorNameList) {
			item.TweenAnimator(time, start, end, easy, isCurrentPos);
		}
	}

	public static VStateAnimator FindAnimator(string nameAnim)
	{
		var list = Resources.FindObjectsOfTypeAll<VStateAnimator>().Where(x => x.isScene = true).ToList();
		var rtn = list.FirstOrDefault(x => x.nameObj == nameAnim);
		return rtn;
	}

	[System.Serializable]
	public struct State
	{
		public Vector3 pos;
		public Vector3 rot;
		public Vector3 scale;

		public State(Vector3 pos, Vector3 rot, Vector3 scale)
		{
			this.pos = pos;
			this.rot = rot;
			this.scale = scale;
		}
	}
}
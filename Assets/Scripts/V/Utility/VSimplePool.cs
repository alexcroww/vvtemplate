﻿using UnityEngine;
using System.Linq;

public class VSimplePool : MonoBehaviour
{
    public string namePool;
    public Transform path;
    public GameObject prefab;
    
    private void OnEnable()
    {
        if (path == null)
            path = transform;
        if (prefab == null && path.childCount>0)
            prefab = path.GetChild(0).gameObject;
    }

    public static GameObject Take(string namPol,bool isActive = false)
    {
        var list = Resources.FindObjectsOfTypeAll<VSimplePool>();
        var pool = list.FirstOrDefault(x => x.namePool == namPol && x.gameObject.scene.name != null);
        GameObject rtnObj = null;
        for (int i = 0; i < pool.path.childCount; i++) {
            if (!pool.path.GetChild(i).gameObject.activeSelf) {
                rtnObj = pool.path.GetChild(i).gameObject;
                break;
            }
        }

        if (rtnObj==null) {
            rtnObj = VLab.SpawnObj(pool.prefab, pool.path);
        }
        
        if(isActive && rtnObj)
            rtnObj.SetActive(true);
        return rtnObj;
    }
}
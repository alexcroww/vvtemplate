﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UniRx;
using Sirenix.OdinInspector;

public class VPool : MonoBehaviour
{
	static VPool inst;

	public static List<Pool> poolList;
	[Header("Настройки")] public float timeAutoClearPool;

	public List<Pool> poolSetupList;

	[System.Serializable]
	public class Pool
	{
		public string name;
		[AssetsOnly] public GameObject prefab;
		public Transform parent;
		public List<VPoolItem> items;
	}

	public static VPool Inst {
		get {
			if (!inst) {
				inst = Resources.FindObjectsOfTypeAll<VPool>().FirstOrDefault();
			}

			return inst;
		}
	}

	void Awake()
	{
		poolList = new List<Pool>(poolSetupList);
		VPoolItem.destroyVPoolItemEvent.Subscribe(RemoveItem);
		if (timeAutoClearPool > 0f)
			Observable.Interval(System.TimeSpan.FromSeconds(10)).TakeUntilDisable(this)
				.Subscribe(_ => ClearNotUseItemPool()).AddTo(this);
	}

	static VPoolItem AddItem(Pool pool)
	{
		var ob = Instantiate(pool.prefab, pool.parent);
		ob.transform.localScale = Vector3.one;
		ob.transform.localPosition = Vector3.zero;
		var itm = ob.GetComponent<VPoolItem>();
		itm.nameItem = pool.name;
		pool.items.Add(itm);
#if UNITY_EDITOR
		itm.gameObject.name = pool.name + "_" + Time.time;
#endif
		return itm;
	}

	public static VPoolItem GetItem(string namePool, bool enable = true)
	{
		var pl = poolList.FirstOrDefault(x => x.name == namePool);
		if (pl == null) {
#if UNITY_EDITOR
			Debug.Log("Не найден пул <color=#ff4444><b>" + namePool + "</b></color>");
#endif
			return null;
		}

		var itm = pl.items.FirstOrDefault(x => !x.isActive.Value);
		if (itm != null) {
#if UNITY_EDITOR
			itm.gameObject.name = namePool + "_" + Time.time;
#endif
			itm.transform.SetAsLastSibling();
			if (enable)
				itm.gameObject.SetActive(true);
			else
				itm.isActive.Value = true;
			return itm;
		}
		else
			return AddItem(pl);
	}

	public static void RemoveItem(VPoolItem itm)
	{
		var pl = poolList.FirstOrDefault(x => x.name == itm.nameItem);
		if (pl == null) {
#if UNITY_EDITOR
			Debug.Log("Не найден пул для удаления объекта <color=#ff4444><b>" + itm.name + "</b></color>");
#endif
			return;
		}

		pl.items.Remove(itm);
		if (!itm.Equals(null))
			Destroy(itm.gameObject);
	}

	public void ClearNotUseItemPool()
	{
		var t = Time.time;
		var list = new List<VPoolItem>();
		foreach (var y in poolList) {
			foreach (var x in y.items) {
				if (!x.gameObject.activeInHierarchy && t - x.lastUseTime >= timeAutoClearPool && !x.isNotDestroy)
					list.Add(x);
			}
		}

		foreach (var x in list) {
			RemoveItem(x);
		}
	}

	public static void ClearAllPool()
	{
		var list = new List<VPoolItem>();
		foreach (var y in poolList) {
			foreach (var x in y.items) {
				list.Add(x);
			}
		}

		foreach (var x in list) {
			RemoveItem(x);
		}
	}
}
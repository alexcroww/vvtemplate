﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class Vfps : MonoBehaviour
{
	Text fpsText;

	void OnEnable()
	{
		fpsText = GetComponent<Text>();
		var frameCount = 0;
		var dt = 0.0;
		var fps = 0.0;
		var updateRate = 4.0;
		Observable.EveryUpdate().TakeUntilDisable(this).Subscribe(_ => {
			frameCount++;
			dt += Time.deltaTime;
			if (dt > 1.0 / updateRate) {
				fps = frameCount / dt;
				frameCount = 0;
				dt -= 1.0 / updateRate;
			}
			fpsText.text = fps.ToString("###");
		}).AddTo(this);
	}
}

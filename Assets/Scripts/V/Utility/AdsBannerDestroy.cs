﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsBannerDestroy : MonoBehaviour
{
	public bool isDestroy;

	void Awake()
	{
		#if UNITY_EDITOR
		if (isDestroy)
			Destroy(gameObject);
		#else
		Destroy(gameObject);
		#endif
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using DG.Tweening;

public class VToast : MonoBehaviour
{
	[SerializeField] CanvasGroup canvasGroup;
	[SerializeField] Text uiText;
	[SerializeField] float showTime = 1f;

	private float animaTime = 0.2f;
	private CompositeDisposable disTimer;

	void Start()
	{
		if(disTimer == null)
			gameObject.SetActive(false);
	}
	
	void SetStart()
	{
		if (canvasGroup == null)
			canvasGroup = GetComponent<CanvasGroup>();

		if (disTimer == null)
			disTimer = new CompositeDisposable();

		if (uiText == null)
			uiText = GetComponentInChildren<Text>();

		canvasGroup.alpha = 0f;
	}

	void ShowToast(string msg)
	{
		gameObject.SetActive(true);
		SetStart();
		disTimer.Clear();
		uiText.text = msg;
		canvasGroup.DOFade(1f, animaTime).SetUpdate(true);
		Observable.Timer(System.TimeSpan.FromSeconds(showTime)).TakeUntilDisable(this)
			.Subscribe(_ => { canvasGroup.DOFade(0f, animaTime).SetUpdate(true); }).AddTo(disTimer);
	}

	public static void Show(string message)
	{
		var toast = Resources.FindObjectsOfTypeAll<VToast>().FirstOrDefault(x => x.gameObject.scene.name != null);
		if (toast != null)
			toast.ShowToast(message);
		else
			Debug.LogError("Не могу найти toast");
	}
}
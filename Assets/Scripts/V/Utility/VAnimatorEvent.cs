﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class VAnimatorEvent : MonoBehaviour
{
	public static Subject<string> eventAnima = new Subject<string>();

	public void SendEvent(string nameEvent)
	{
		eventAnima.OnNext(nameEvent);
	}
}

﻿using UniRx;
using UnityEngine;

public static class Rx 
{
    public static IObservable<Unit> Timer(double dueTime)
    {
        return Observable.Timer(System.TimeSpan.FromSeconds(dueTime)).AsUnitObservable();
    }
    
    public static IObservable<Unit> Timer(double dueTime, MonoBehaviour mb)
    {
        return Observable.Timer(System.TimeSpan.FromSeconds(dueTime))
            .TakeUntilDisable(mb)
            .AsUnitObservable();
    }
    
    public static IObservable<Unit> Interval(double period)
    {
        return Observable.Interval(System.TimeSpan.FromSeconds(period)).AsUnitObservable();
    }
}

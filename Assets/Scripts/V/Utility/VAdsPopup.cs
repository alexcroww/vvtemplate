﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UnityEngine.Advertisements;
using System.Linq;
using Ads;

public class VAdsPopup : MonoBehaviour
{
	public bool isScene;
	public Text messageText;

	public static Subject<bool> eventOk = new Subject<bool>();
	[HideInInspector] public int typePopup;

	ISubject<Ads.ShowResult> getResult = new Subject<Ads.ShowResult>();

	ISubject<UnityEngine.Advertisements.ShowResult> getResultUnity =
		new Subject<UnityEngine.Advertisements.ShowResult>();

	public static CompositeDisposable disPopup = new CompositeDisposable();
	private float tempTimeScale;
	public static bool isOpen;

	public static void Init(int num = 3, int reward = 0, int cost = 0, int coin = 0)
	{
		var popup = Resources.FindObjectsOfTypeAll<VAdsPopup>().FirstOrDefault(x => x.gameObject.scene.name != null);
		if (popup != null)
			popup.init(num, reward, cost, coin);
		else
			Debug.LogError("Не могу найти popup с рекламой");
	}

	public void init(int num, int reward, int cost, int coin)
	{
		isOpen = true;
		tempTimeScale = Time.timeScale;
		Time.timeScale = 0f;

		disPopup.Clear();

		gameObject.SetActive(true);
		typePopup = num;

		eventOk.Subscribe(_ => gameObject.SetActive(false)).AddTo(disPopup);

		var langRU = Application.systemLanguage == SystemLanguage.Russian;
		if (num == 0)
			messageText.text = langRU
				? "Посмотреть рекламу и получить <b>" + reward + "</b> монет?"
				: "View ads and get <b>" + reward + "</b> coins?";
		if (num == 1)
			messageText.text = langRU
				? "Не хватает <b>" + (cost - coin) + "</b> монет." + "\nПосмотреть рекламу и получить <b>" + reward +
				  "</b> монет?"
				: "Not enough <b>" + (cost - coin) + "</b> coins." + "\nView ads and get <b>" + reward + "</b> coins?";
		if (num == 2)
			messageText.text = langRU
				? "Купить выбранный элемент за <b>" + cost + "</b> монет?"
				: "Buy the selected item for <b>" + cost + "</b> coins?";
//		if (num == 3)
//			messageText.text = langRU ? "Открыть новый элемент за просмотр рекламы?"
//					: "Open new element for viewing ads?";
		if (num == 3)
			messageText.text = langRU
				? "Продолжить игру за просмотр рекламы?"
				: "Continue the game for viewing ads?";
	}

	void OnDisable()
	{
		isOpen = false;
		Time.timeScale = tempTimeScale;
		gameObject.SetActive(false);
	}

	public void BtnNo()
	{
		VSound.ClickBack();
		gameObject.SetActive(false);
	}

	public void BtnYes()
	{
		VSound.Click();
		if (typePopup != 2) {
			if (AdmobController.isRewardLoaded && !Stats.Inst.stats.isTestMode) {
#if UNITY_EDITOR
				getResult.OnNext(Ads.ShowResult.Finished);
				eventOk.OnNext(true);
				ShowTest();
#endif

				getResult
					.Take(1)
					.Where(x => x == Ads.ShowResult.Finished)
					.ObserveOnMainThread()
					.Subscribe(x => eventOk.OnNext(true))
					.AddTo(disPopup);
				getResult
					.Take(1)
					.Where(x => x == Ads.ShowResult.Skipped)
					.ObserveOnMainThread()
					.Subscribe(x => gameObject.SetActive(false))
					.AddTo(disPopup);
				AdmobController.ShowReward(getResult.OnNext);
			}
			else if (Advertisement.IsReady("rewardedVideo") && !Stats.Inst.stats.isTestMode) {
				var options = new ShowOptions {resultCallback = getResultUnity.OnNext};
				getResultUnity.Take(1).Subscribe(x =>
				{
#if UNITY_IOS
					AudioListener.pause = false;
				#endif
					if (x == UnityEngine.Advertisements.ShowResult.Finished)
						eventOk.OnNext(true);
				}).AddTo(disPopup);
#if UNITY_IOS
					AudioListener.pause = true;
				#endif
				Advertisement.Show("rewardedVideo", options);
			}
			else {
				gameObject.SetActive(false);


#if UNITY_EDITOR
				ShowTest();
#endif
				if (Stats.Inst.stats.isTestMode) {
					VToast.Show("Награда за рекламу.\nВ тестовом режиме");
					getResult.OnNext(Ads.ShowResult.Finished);
					eventOk.OnNext(true);
				}
				else
					VToast.Show(LanguageController.GetString("unity_failed"));
			}
		}
		else {
			eventOk.OnNext(true);
		}
	}

	void ShowTest()
	{
		var cnvs = new GameObject();
		cnvs.AddComponent<Canvas>();
		cnvs.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
		var img = new GameObject();
		img.transform.SetParent(cnvs.transform);
		img.AddComponent<Image>();
		img.GetComponent<RectTransform>().anchorMin = Vector2.zero;
		img.GetComponent<RectTransform>().anchorMax = Vector2.one;
		img.transform.localPosition = Vector3.zero;
		img.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0.9f);
		var txt = new GameObject();
		txt.transform.SetParent(cnvs.transform);
		txt.AddComponent<Text>();
		txt.transform.localScale = Vector3.one;
		txt.transform.localPosition = Vector3.zero;
		txt.GetComponent<RectTransform>().anchorMin = Vector2.zero;
		txt.GetComponent<RectTransform>().anchorMax = Vector2.one;
		txt.GetComponent<Text>().font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		txt.GetComponent<Text>().text = "ADS SHOW";
		txt.GetComponent<Text>().fontSize = 50;
		txt.GetComponent<Text>().color = Color.green;
		txt.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
		Observable.Timer(System.TimeSpan.FromSeconds(0.3f), Scheduler.MainThreadIgnoreTimeScale)
			.Subscribe(_ => Destroy(cnvs));
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) {
			gameObject.SetActive(false);
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Linq;

public class VPoolItem : MonoBehaviour
{
	
	[Header("Настройки")]
	public string nameItem;
	public bool isNotDestroy;

	[Header("Парметры")]
	public BoolReactiveProperty isActive;

	[HideInInspector]
	public float lastUseTime;

	public static Subject<VPoolItem> destroyVPoolItemEvent = new Subject<VPoolItem>();

	void Awake()
	{
		isActive.Where(b => !isNotDestroy).Subscribe(_ => lastUseTime = Time.time).AddTo(this);
	}

	void OnEnable()
	{
		isActive.Value = true;
	}

	void OnDisable()
	{
		isActive.Value = false;
	}

	public void DestroyItem()
	{
		destroyVPoolItemEvent.OnNext(this);
		Destroy(this);
	}
}
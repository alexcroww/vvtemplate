﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using DG.Tweening;
using Random = UnityEngine.Random;

public class VAnimator : MonoBehaviour
{
	public bool isBadCutSize;
	public int loopCount;
	[HideInInspector] public Image image;
	public List<Frame> frameList;

	[HideInInspector] public int currentFrame;
	[HideInInspector] public int currentLoopCount;

	CompositeDisposable dis;

	[System.Serializable]
	public class Frame
	{
		public Sprite sprite;
		public float timeMin, timeMax;
	}

	void OnEnable()
	{
		dis = new CompositeDisposable();
		if (image == null)
			image = GetComponent<Image>();
		currentFrame = 0;
		currentLoopCount = 0;
		image.color = Color.white;
		PlayAnima();
	}

	void OnDisable()
	{
		StopAnima();
	}

	public void StopAnima()
	{
		dis.Clear();
	}

	public void PlayAnima()
	{
		image.sprite = frameList[currentFrame].sprite;
		if (isBadCutSize)
			image.SetNativeSize();

		Observable.Timer(
				TimeSpan.FromSeconds(Random.Range(frameList[currentFrame].timeMin, frameList[currentFrame].timeMax)))
			.Subscribe(_ =>
			{
				currentFrame++;
				if (currentFrame >= frameList.Count) {
					currentFrame = 0;
					currentLoopCount++;
					if (loopCount != 0 && currentLoopCount >= loopCount) {
						image.DOFade(0f, 0.2f);
						Observable.Timer(TimeSpan.FromSeconds(0.2f)).Subscribe(__ => { gameObject.SetActive(false); });
						return;
					}
				}

				PlayAnima();
			}).AddTo(dis);
	}
}
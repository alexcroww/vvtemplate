﻿using UnityEngine;

namespace V {
	public static class VExt 
	{
		public static Vector2 xy(this Vector3 v)
		{
			return new Vector2(v.x, v.y);
		}
	}
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationLoading : MonoBehaviour
{
	public List<Oval> images;
	public Vector2 sinCosV2;
	public float sizeCircle = 2;

	[System.Serializable]
	public struct Oval
	{
		public Image img;
		public float speed;
	}

	void Update()
	{
		images.ForEach(v =>
		{
//            v.img.transform.Rotate(Vector3.back * v.speed);
			v.img.transform.localPosition = Vector3.up * Mathf.Cos(v.img.transform.position.x) * sinCosV2.y +
			                                Vector3.right * Mathf.Cos((6f + Time.time) * v.speed) * sinCosV2.x;
			v.img.transform.localScale = Vector3.one * Mathf.Cos(v.img.transform.position.x) * sizeCircle;
		});
	}
}
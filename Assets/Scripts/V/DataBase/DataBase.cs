﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;
using System.Linq;

public class DataBase : ScriptableObject
{
	public int coinForAds = 50;
	public List<LockElement> lockIdList;

	[BoxGroup("Размеры для генерации таблицы")]
	public int a, b, c;
	
	[Serializable]
	public class LockElement
	{
		public string idLockItem;

		[Tooltip("0_Открыто -1_Закрыто рекламой 2_Закрыто монетой")]
		public int cost;

		public LockElement(string idLockItem, int cost)
		{
			this.idLockItem = idLockItem;
			this.cost = cost;
		}
	}

	public bool IsContains(string id)
	{
		return lockIdList.Any(x => x.idLockItem == id);
	}
}
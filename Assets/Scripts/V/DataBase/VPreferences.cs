﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class VPreferences
{
	static VPreferences()
	{
		soundEnabled
			.DoOnSubscribe(() => soundEnabled.Value = PlayerPrefs.GetInt("SoundEnabled", 1) > 0)
			.Select(b => b ? 1 : 0)
			.Subscribe(i => PlayerPrefs.SetInt("SoundEnabled", i));

		musicEnabled
			.DoOnSubscribe(() => musicEnabled.Value = PlayerPrefs.GetInt("MusicEnabled", 1) > 0)
			.Select(b => b ? 1 : 0)
			.Subscribe(i => PlayerPrefs.SetInt("MusicEnabled", i));

		vibrationEnabled
			.DoOnSubscribe(() => vibrationEnabled.Value = PlayerPrefs.GetInt("VibrationEnabled", 1) > 0)
			.Select(b => b ? 1 : 0)
			.Subscribe(i => PlayerPrefs.SetInt("VibrationEnabled", i));

		cameraEnabled
			.DoOnSubscribe(() => cameraEnabled.Value = PlayerPrefs.GetInt("CameraEnabled", 0) > 0)
			.Select(b => b ? 1 : 0)
			.Subscribe(i =>
			{
				PlayerPrefs.SetInt("CameraEnabled", i);
				lightEnabled.Value &= !cameraEnabled.Value;
			});

		lightEnabled
			.DoOnSubscribe(() => lightEnabled.Value = PlayerPrefs.GetInt("LightEnabled", 1) > 0)
			.Select(b => b ? 1 : 0)
			.Subscribe(i =>
			{
				PlayerPrefs.SetInt("LightEnabled", i);
				cameraEnabled.Value &= !lightEnabled.Value;
			});

		sleepEnabled
			.DoOnSubscribe(() => sleepEnabled.Value = PlayerPrefs.GetInt("SleepEnabled", 0) > 0)
			.Select(b => b ? 1 : 0)
			.Subscribe(i =>PlayerPrefs.SetInt("SleepEnabled", i));
	}

	public static BoolReactiveProperty soundEnabled = new BoolReactiveProperty(true);
	public static BoolReactiveProperty musicEnabled = new BoolReactiveProperty(true);
	public static BoolReactiveProperty vibrationEnabled = new BoolReactiveProperty(true);
	public static BoolReactiveProperty cameraEnabled = new BoolReactiveProperty(false);
	public static BoolReactiveProperty lightEnabled = new BoolReactiveProperty(true);
	public static BoolReactiveProperty sleepEnabled = new BoolReactiveProperty(false);
}
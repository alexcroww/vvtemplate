﻿using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;

public class Stats : MonoBehaviour
{
	static Stats _inst;
	[InlineEditor] public StatsBase stats;

	public static Stats Inst {
		get {
			if (!_inst) {
				_inst = Resources.FindObjectsOfTypeAll<Stats>().FirstOrDefault(x => x.gameObject.scene.name != null);
			}

			return _inst;
		}
	}

	void Start()
	{
		if (Stats.Inst == this)
			DontDestroyOnLoad(gameObject);
		else
			Destroy(gameObject);
	}
}
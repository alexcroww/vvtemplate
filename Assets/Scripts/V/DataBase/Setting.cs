﻿/*
version 0.6.0
*/
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UniRx;
using Sirenix.OdinInspector;

public class Setting : MonoBehaviour
{
	static Setting _inst;
	[InlineEditor]
	public AccountBase ab;
	[InlineEditor]
	public DataBase db;

	public IntReactiveProperty coin;
	public int choose;
	public bool isGodMode;

	[Header("Настройки")] 
	public FloatReactiveProperty speed;
	public FloatReactiveProperty size;
	
	public static Setting Inst {
		get {
			if (!_inst) {
				_inst = Resources.FindObjectsOfTypeAll<Setting>().FirstOrDefault(x => x.gameObject.scene.name != null);
			}

			return _inst;
		}
	}

	void Start()
	{
		if (Setting.Inst == this)
			DontDestroyOnLoad(gameObject);
		else
			Destroy(gameObject);

		isGodMode = false;
	}

	public void Init()
	{
		LoadProgress();
		coin.Value = PlayerPrefs.GetInt("coin", 0);
		coin.Subscribe(x => PlayerPrefs.SetInt("coin", coin.Value)).AddTo(this);
	}

	public void LoadProgress()
	{
		if (!PlayerPrefs.HasKey("version")) {
			PlayerPrefs.SetInt("version", 0);
			PlayerPrefs.SetInt("coin", 0);

			for (int i = 0; i < db.lockIdList.Count; i++) {
				PlayerPrefs.SetInt(db.lockIdList[i].idLockItem, db.lockIdList[i].cost != 0 ? 1 : 0);
			}
		}
	}

	public bool GetOpenItem(int chooseNum, int type, int item)
	{
		var id = chooseNum + "_" + type + "_" + item;
		return isGodMode || PlayerPrefs.GetInt(id, 0) == 0;
	}

	public bool GetOpenItem(string idText)
	{
		return isGodMode || PlayerPrefs.GetInt(idText, 0) == 0;
	}

	public int GetCostItem(int a, int b, int c)
	{
		var rtn = db.lockIdList.FirstOrDefault(x => x.idLockItem == a + "_" + b + "_" + c);
		if (rtn != null)
			return rtn.cost;

		Debug.LogWarning("Цена не найдена <b>" + a + "_" + b + "_" + c + "</b>");
		return 0;
	}

	public int GetCostItem(string idText)
	{
		var rtn = db.lockIdList.FirstOrDefault(x => x.idLockItem == idText);
		if (rtn != null)
			return rtn.cost;

		Debug.LogWarning("Цена не найдена <b>" + idText + "</b>");
		return 0;
	}

	public void OnLockSaveItem(int chooseNum, int type, int item, bool isOpen = true)
	{
		var id = chooseNum + "_" + type + "_" + item;
		PlayerPrefs.SetInt(id, isOpen ? 0 : 1);
	}

	public void OnLockSaveItem(string idText, bool isOpen = true)
	{
		PlayerPrefs.SetInt(idText, isOpen ? 0 : 1);
	}
	
	public void OnLockAllItem()
	{
		isGodMode = true;
	}

#if UNITY_EDITOR
	[ContextMenu("Заполнить таблицу")]
	void SetID()
	{
		if (db.lockIdList == null || db.lockIdList.Count == 0)
			db.lockIdList = new List<DataBase.LockElement>();
		var list = db.lockIdList;
		for (int a = 0; a < db.a; a++) {
			for (int b = 0; b < db.b; b++) {
				for (int c = 0; c < db.c; c++) {
					if (!db.IsContains(a + "_" + b + "_" + c))
						list.Add(new DataBase.LockElement(a + "_" + b + "_" + c, -1));
				}
			}
		}
	}
#endif
}

/*[Serializable]
public class DataSave
{
	public List<ItemOpen> itemOpens;

	[Serializable]
	public class ItemOpen
	{
		public string id;
		public bool isOpen;

		public ItemOpen(string id, bool isOpen)
		{
			this.id = id;
			this.isOpen = isOpen;
		}
	}
}*/
﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Sirenix.OdinInspector;

public class StatsBase : ScriptableObject
{
	[ToggleLeft] [LabelText("Тестовый режим"), GUIColor("IsTest")]
	public bool isTestMode;

	[FoldoutGroup("Основное")] [LabelText("Макс. угол поворота оружия")]
	public float angleWeapon = 80f;
	
	[LabelText("Оружия")] [ListDrawerSettings(ShowIndexLabels = true, ListElementLabelName = "subText")]
	public List<WeaponS> weapons;

	[LabelText("Уровни")] [ListDrawerSettings(ShowIndexLabels = true)]
	public List<LevelSet> levels;

	Color IsTest()
	{
		return isTestMode ? new Color(1f,0.2f,0.2f) : new Color(0.2f, 1f, 0.2f);
	}

	[System.Serializable]
	public class WeaponS
	{
		[HideLabel] public string subText;

		[LabelText("Патроны")] [ProgressBar(0, 20, 0.8f, 0.8f, 0.4f)]
		public int ammoFull = 10;

		[LabelText("Время между выстрелами")] [Range(0f, 1f)]
		public float timeShot = 0.1f;
	}

	[System.Serializable]
	public class LevelSet
	{
		[Range(10,100)] [LabelText("Скорость")] [GUIColor(0.6f, 0.6f, 1f)]
		public int speed;
		
		[MinMaxSlider(0, 10, true)] [LabelText("Фигуры")] [GUIColor(0.6f, 1f, 0.6f)]
		public Vector2Int objRange;
	}
}
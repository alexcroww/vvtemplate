﻿using UnityEngine;
using System;
using System.Linq;

public class AccountBase : ScriptableObject
{
	public AccountName accountName;
	public string urlAppMarket = "ссылка на аппу";

	public enum AccountName
	{
		VVGames
	}
	
	[Serializable]
	public class Accounts
	{
		public AccountName code;
		public string[] value;

		public Accounts(AccountName code, string[] value)
		{
			this.code = code;
			this.value = value;
		}
	}

	public Accounts GetAccounts()
	{
		if (accounts.Any(x => x.code == accountName)) {
			return accounts.FirstOrDefault(x => x.code == accountName);
		}
		else {
			Debug.LogError("Не заданн аккаунт");
			return accounts[0];
		}
	}

	//TODO: Make enum
	public Accounts[] accounts = {
		new Accounts(AccountName.VVGames, new string[] {
			/*Package*/ "com.vvgames.*",
			/*Account link*/ "VVGames",
			/*Hashtag*/ "#vvgames",
			/*Popup style*/ "white",
			/*Privacy Policy*/
			"https://docs.google.com/document/d/1F78sIZK8d8DeBcUmxq3nab7GS7uMfrADrkf03EFpE1s/edit?usp=sharing"
		})
	};
}
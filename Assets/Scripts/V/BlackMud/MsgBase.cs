﻿using UnityEngine;

public class MsgBase
{
	public MonoBehaviour sender { get; private set; }
	public int id { get; private set; }
	public System.Object data { get; private set; }

	public MsgBase(MonoBehaviour sender, int id, System.Object data)
	{
		this.sender = sender;
		this.id = id;
		this.data = data;
	}

	public static MsgBase Create(MonoBehaviour sender, int id, System.Object data)
	{
		return new MsgBase(sender, id, data);
	}
}
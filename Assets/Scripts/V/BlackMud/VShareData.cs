﻿using UnityEngine;

public class VShareData : MonoBehaviour {
	public const int MSG_ADD_MOVE = 1001;
	public const int MSG_OFF_PLATFORM = 1002;
	public const int MSG_X_PLAYER_POSITION = 1003;
}

/*
MessageBroker.Default.Receive<MsgBase>().Where(msg => msg.id == VShareData.MSG_CLICK_MACHINE)
			.Subscribe(msg =>
			{
				float data = (float) msg.data;
				if (damageCD <= 0f)
					damageCD = Stats.Inst.stats.cdTimeDamage;
			}).AddTo(this);
			
MessageBroker.Default.Publish(MsgBase.Create(this, VShareData.MSG_CLICK_MACHINE, 1f));
*/
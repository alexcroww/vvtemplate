﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text.RegularExpressions;

public class VLab
{
	/// <summary>Поиск объекта в Transform</summary>
	/// <param name="trans">Родитель</param>
	/// <param name="name">Имя объекта</param>
	public static GameObject FindChildObj(Transform trans, string name)
	{
		var rtrn = trans.GetComponentsInChildren<Transform>(true).FirstOrDefault(x => x.name == name);
		return rtrn ? rtrn.gameObject : null;
	}

	//Парсинг информации в строке с разделителем
	public static List<string> ParseString(string sourceText, string separator = "|")
	{
		var rtrnL = new List<string>();
		var itemString = "";

		if (sourceText == "") {
			Debug.LogError("Источник, пустая строка");
			return rtrnL;
		}

		for (int i = 0; i < sourceText.Length; i++) {
			var c = sourceText[i].ToString();
			if (c == separator) {
				rtrnL.Add(itemString);
				itemString = "";
				continue;
			}
			if (itemString != "" || c != " ") {
				itemString += c;
				if (i == sourceText.Length - 1)
					rtrnL.Add(itemString);
			}
		}
		return rtrnL;
	}

	//Удаление дочерних объектов
	public static void DestroyAllChild(Transform trans, GameObject exceptObj = null)
	{
		var dList = new List<GameObject>();
		for (int i = 0; i < trans.childCount; i++)
			if (!exceptObj || exceptObj != trans.GetChild(i).gameObject)
				dList.Add(trans.GetChild(i).gameObject);

		for (int i = 0; i < dList.Count; i++)
			GameObject.Destroy(dList[i]);
	}

	//Перемешать лист
	public static void MixList(IList list)
	{
		for (int i = 0; i < list.Count; i++) {
			var rand = Random.Range(0, list.Count);
			var buf = list[i];
			list[i] = list[rand];
			list[rand] = buf;
		}
	}

	//Вывод времени
	public static string TimeString(float time, int kol = 1)
	{
		switch (kol) {
			case 0:
				return time.ToString("0");
			case 1:
				return Mathf.Floor(time / 60).ToString("00") + ":" + ((int)time / 1 % 60).ToString("00");
			case 2:
				return Mathf.Floor(time / 60).ToString("00") + ":" + ((int)time / 1 % 60).ToString("00") + ":" + (time % 1.0f * 100).ToString("00");
			default :
				return time.ToString();
		}
	}

	//Instantiate новых объектов
	public static GameObject SpawnObj(GameObject obj, Transform parent, Vector3 pos, Vector3 scale, bool isLocal)
	{
		var ob = GameObject.Instantiate(obj, parent);
		ob.transform.localScale = scale;
		ob.transform.localRotation = Quaternion.identity;
		if (isLocal)
			ob.transform.localPosition = pos;
		else
			ob.transform.position = pos;
		return ob;
	}

	public static GameObject SpawnObj(GameObject obj, Transform parent)
	{
		var ob = GameObject.Instantiate(obj, parent);
		ob.transform.localScale = Vector3.one;
		ob.transform.localPosition = Vector3.zero;
		ob.transform.localRotation = Quaternion.identity;
		return ob;
	}

	//Рандом из коллекции
	//	public static T randomElement<T>(this ICollection<T> list)
	//	{
	//		return list.Count < 1 ? default(T) : list.ElementAt(Random.Range(0, list.Count));
	//	}
	public static T RandomElement<T>(IList<T> list)
	{
		return list.Count < 1 ? default(T) : list.ElementAt(Random.Range(0, list.Count));        
	}

	public static string StringFloat(string source)
	{
		source = Regex.Replace(source, "[^0-9\\.]", "");
		source = new Regex(@"[0-9]*\.?[0-9]*").Match(source).Value;
		return source;
	}

	public static Color Alpha(Color col, float alpha = 1f)
	{
		return new Color(col.r, col.g, col.b, alpha);
	}

	public static float AngleForTwoPoint(Vector2 a, Vector2 b, float dopAngle = 0f)
	{
		return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg + dopAngle;
	}

	//Рандом вероятности
	public static bool Rbool(float v = 0.5f)
	{
		return Random.value <= v;
	}
	
	//эм, для чего не помню, но пусть будет
	public static int FirstSubling(IList<Transform> list)
	{
		var indexList = list.Select(item => item.GetSiblingIndex()).ToList();
		var minSub = indexList.Min(x => x);
		return list.IndexOf(list.FirstOrDefault(x => x.GetSiblingIndex() == minSub));
	}
}

﻿/*
version 1.2
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UniRx;
using DG.Tweening;

public class VUIAnimator : MonoBehaviour
{
	public static float TIME_DEFAULT = 0.5f;

	public bool isScene = true;
	public bool isDefualt;
	public string nameAnimator;
	
	public List<State> stateList;

	[HideInInspector]
	public IntReactiveProperty state;
	[HideInInspector]
	public Image image;
	[HideInInspector]
	public RectTransform rectTrans;

	public Tween posTween, scaleTween, colorTween;

	void OnEnable()
	{
		Init();
	}

	public void Init()
	{
		if (image == null) {
			image = GetComponent<Image>();
			rectTrans = image.rectTransform;
			state.SetValueAndForceNotify(0);
			isScene = gameObject.scene.name != null;
		}

		if (!isDefualt) {
			SetDefaultAnimation();
			isDefualt = true;
		}
		
		state.TakeUntilDisable(gameObject).Subscribe(Animation).AddTo(this);
	}

	public void Animation(int x)
	{
		if (stateList[state.Value].active)
			gameObject.SetActive(true);

		if (posTween != null && posTween.IsPlaying())
			posTween.Pause();
		if (scaleTween != null && scaleTween.IsPlaying())
			scaleTween.Pause();
		if (colorTween != null && colorTween.IsPlaying())
			colorTween.Pause();

		var eas = (stateList.Count > x ? stateList[x].ease : DOTween.defaultEaseType);
		var tim = (stateList.Count > x ? stateList[x].time : TIME_DEFAULT);
		if (stateList.Count > x && stateList[x].pos != Vector2.one * -1)
			posTween = rectTrans.DOAnchorPos(stateList[x].pos, tim).SetEase(eas);
		if (stateList.Count > x && stateList[x].scale != Vector3.one * -1)
			scaleTween = rectTrans.DOScale(stateList[x].scale, tim).SetEase(eas);
		if (stateList.Count > x && stateList[x].color != new Color())
			colorTween = image.DOColor(stateList[x].color, tim).SetEase(eas);

		posTween.OnComplete(() => {
			if (!stateList[state.Value].active && Application.isPlaying)
				gameObject.SetActive(false);
		});
	}

	public void SetDefaultAnimation()
	{
		rectTrans.anchoredPosition = stateList[0].pos;
		rectTrans.localScale = stateList[0].scale;
		image.color = stateList[0].color;
		state.Value = 0;
	}
	
	public static void SetState(string nameAnimator, int stateAnimator)
	{
		var list = Resources.FindObjectsOfTypeAll<VUIAnimator>().Where(x => x.isScene = true).ToList();
		var animatorNameList = list.FindAll(x => x.nameAnimator == nameAnimator);

		foreach (var item in animatorNameList) {
			item.gameObject.SetActive(true);
			item.state.Value = stateAnimator;
		}
	}

	public static void SetDefault(string nameAnim)
	{
		var list = Resources.FindObjectsOfTypeAll<VUIAnimator>().Where(x => x.nameAnimator == nameAnim && x.stateList.Count > 0).ToList();

		foreach (var item in list) {
			item.rectTrans.anchoredPosition = item.stateList[0].pos;
			item.rectTrans.localScale = item.stateList[0].scale;
			item.image.color = item.stateList[0].color;
			item.state.Value = 0;
		}
	}

	[System.Serializable]
	public struct State
	{
		[SerializeField]
		public float time;
		public Vector2 pos;
		public Vector3 scale;
		public Color color;
		public Ease ease;
		public bool active;
		[HideInInspector]
		public Vector3 posWorld;

		public State(float time, Vector2 pos, Vector3 scale, Color color, Ease ease, bool active, Vector3 posWorld)
		{
			this.time = time;
			this.pos = pos;
			this.scale = scale;
			this.color = color;
			this.ease = ease;
			this.active = active;
			this.posWorld = posWorld;
		}
	}
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System.Linq;
using UnityEngine.UI;

public class VButton : MonoBehaviour
{
	public static List<VButton> listObject = new List<VButton>();

	[Header("Настройки")]
	public string nameObject;
	public int id;
	public bool isAutoChangeState;
	public IntReactiveProperty state;
	public List<string> soundNames;

	[Header("Ресурсы")]
	public List<Sprite> sprites;

	[HideInInspector]
	public Image image;
	[HideInInspector]
	public Button btn;
	Color startColor;

	public static Subject<VButton> onClick = new Subject<VButton>();

	void OnEnable()
	{
		SetOnEnable();
	}

	public void SetOnEnable()
	{
		if (image == null) {
			image = GetComponent<Image>();
			if (image)
				startColor = image.color;
			else
				btn = GetComponent<Button>();
		}

		if (image != null) {
			state.TakeUntilDisable(gameObject).Subscribe(x => {
				if (sprites.Count > x)
					image.sprite = sprites[x];
				else {
					var cK = (1f / (1f + x));
//					image.color = new Color(startColor.r / cK, startColor.g / cK, startColor.b / cK);
				}
			});

			image.OnPointerClickAsObservable().TakeUntilDisable(gameObject).Subscribe(_ => {
				if (soundNames.Count > state.Value && soundNames[state.Value] != "-")
					VSound.Play((soundNames.Count <= state.Value || soundNames[state.Value] == "") ? VSound.TAP_CLIP_NAME : soundNames[state.Value]);

				if (!isAutoChangeState) {
					onClick.OnNext(this);
				} else
					NextState();
			});
		} else {
			btn.OnPointerClickAsObservable().TakeUntilDisable(gameObject).Subscribe(_ => {
				if (soundNames.Count > state.Value && soundNames[state.Value] != "-")
					VSound.Play((soundNames.Count <= state.Value || soundNames[state.Value] == "") ? VSound.TAP_CLIP_NAME : soundNames[state.Value]);

				onClick.OnNext(this);
			});
		}
	}

	public void NextState()
	{
		if (state.Value + 1 >= sprites.Count && state.Value != 0)
			state.Value = 0;
		else
			state.Value++;
	}

	public static VButton GetButton(string nameObject)
	{
		//todo id and isScene
		var rtn = listObject.FirstOrDefault(x => x.nameObject == nameObject);
		if (rtn == null) {
			var list = Resources.FindObjectsOfTypeAll<VButton>();
			rtn = list.FirstOrDefault(x => x.nameObject == nameObject);
			if (rtn != null)
				listObject.Add(rtn);
			else
				Debug.LogError("Кнопки с именем <b>" + nameObject + ">/b> не существует");
		}
		return rtn;
	}

	public static void AsObservable(string name, Action<VButton> action)
	{
		onClick.Where(x => x.nameObject == name).Subscribe(action);
	}
}
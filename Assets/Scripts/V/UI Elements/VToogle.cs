﻿using System.Linq;
using UnityEngine;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

public class VToogle : MonoBehaviour
{
	public string nameToggle;
	public BoolReactiveProperty isOn;
	public Image mainImage;
	public RectTransform pickerRect;
	public Vector2 moveXRange;
	public float timeAnimation = 0.3f;
	public CanvasGroup textOnCanvasGroup;
	public CanvasGroup textOffCanvasGroup;

	void Awake()
	{
		isOn.Value = PlayerPrefs.GetInt(nameToggle, isOn.Value ? 1 : 0) > 0;
	}

	void OnEnable()
	{
		mainImage.OnPointerDownAsObservable().TakeUntilDisable(this).Subscribe(_ => { isOn.Value = !isOn.Value; })
			.AddTo(this);
		
		isOn.TakeUntilDisable(this).Subscribe(b => ShowState(b)).AddTo(this);
	}

	public void ShowState(bool on, bool isAnmimation = true)
	{
		PlayerPrefs.SetInt(nameToggle, on ? 1 : 0);
		pickerRect.DOAnchorPosX(on ? moveXRange.y : moveXRange.x, isAnmimation ? timeAnimation : 0f).SetId(nameToggle);
		textOnCanvasGroup.DOFade(on ? 1f : 0f, isAnmimation ? timeAnimation : 0f).SetId(nameToggle);
		textOffCanvasGroup.DOFade(on ? 0f : 1f, isAnmimation ? timeAnimation : 0f).SetId(nameToggle);
	}

	public static VToogle GetToggle(string nameObject)
	{
		var list = Resources.FindObjectsOfTypeAll<VToogle>();
		var rtn = list.FirstOrDefault(x => x.nameToggle == nameObject && x.gameObject.scene.name != null);
		if (rtn == null)
			Debug.LogError("Переключателя с именем <b>" + nameObject + ">/b> не существует");

		return rtn;
	}
}
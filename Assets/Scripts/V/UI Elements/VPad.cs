﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx;

public class VPad : MonoBehaviour
{
	[Header("UI и объекты")]
	public RectTransform padRect;

	[Header("Настройки")]
	public int showType = 0;
	public int hideType = 0;
	public bool isShowOnEnable;
	public float timeAnima = 0.3f;
	public Vector3 showV3;
	public Vector3 hideV3;

	[Header("Параметры")]
	public BoolReactiveProperty isOpen;
	public BoolReactiveProperty isShow;

	Tweener showTween, hideTween;

	public void OnEnableInScene()
	{
		isShow.TakeUntilDisable(this).Subscribe(padRect.gameObject.SetActive);

		if (!isShowOnEnable)
			Hide(0);
		else
			Show(0);
	}

	public void BtnShow(int type)
	{
		isShow.TakeUntilDisable(this).Subscribe(padRect.gameObject.SetActive);
//		VSound.Tap();
		VSound.Play("popupOpen");
		Show(type != -1 ? type : showType);
	}

	public void BtnHide(int type)
	{
//		VSound.Tap();
		VSound.Play("popupClose");
		Hide(type != -1 ? type : hideType);
	}

	void Show(int type)
	{
		switch (type) {
			case 0://без анимации
				isOpen.Value = true;
				isShow.Value = true;
				padRect.anchoredPosition = showV3;
				break;
			case 1://триггер
				if (!isOpen.Value) {
					isShow.Value = true;
					if (showTween != null)
						showTween.Kill();
					showTween = padRect.DOAnchorPos(showV3, timeAnima).OnComplete(() => isOpen.Value = true);
				} else
					Hide(1);
				break;
		}

	}

	void Hide(int type)
	{

		switch (type) {
			case 0://без анимаций
				padRect.anchoredPosition = hideV3;
				isOpen.Value = false;
				isShow.Value = false;
				break;
			case 1://триггер
				if (hideTween != null)
					hideTween.Kill();
				hideTween = padRect.DOAnchorPos(hideV3, timeAnima).OnComplete(() => {
					isOpen.Value = false;
					isShow.Value = false;
				});
				break;
		}
	}
}
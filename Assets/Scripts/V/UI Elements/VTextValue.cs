﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Linq;

public class VTextValue : MonoBehaviour
{
	public string nameText;
	public FloatReactiveProperty value;
	public StringReactiveProperty valueString;
	public bool isOtherType;
	public float timeChange;
	public FormatValue format;

	private float timeTick = 0.05f;
	Text text;
	CompositeDisposable dis, disAnima;

	public enum FormatValue
	{
		i = 0,
		f = 1,
		s = 2
	}

	void OnEnable()
	{
		dis = new CompositeDisposable();
		disAnima = new CompositeDisposable();
		text = GetComponent<Text>();
		if (!isOtherType)
			value.Pairwise().Subscribe(x =>
			{
				if (format == FormatValue.i) {
					if (timeChange > 0f)
						AnimationValue(x.Previous, x.Current);
					else
						text.text = x.Current.ToString("0");
				}

				if (format == FormatValue.f)
					text.text = x.Current.ToString("F");
				if (format == FormatValue.s)
					text.text = x.Current.ToString();
			}).AddTo(dis);

		if (isOtherType)
			valueString.Subscribe(x => text.text = valueString.Value).AddTo(dis);

		value.SetValueAndForceNotify(value.Value);
	}

	void OnDisable()
	{
		dis.Clear();
		disAnima.Clear();
	}

	public static VTextValue FindText(string nameText)
	{
		var list = Resources.FindObjectsOfTypeAll<VTextValue>();
		var rtn = list.FirstOrDefault(x => x.nameText == nameText);
		return rtn;
	}

	void AnimationValue(float old, float cur)
	{
		disAnima.Clear();
		var step = (cur - old) / (timeChange / timeTick);

		if (format == FormatValue.i) {
			if (Mathf.Abs(old - cur) <= 1f)
				old = cur;
			text.text = old.ToString("0");
			Observable.Interval(System.TimeSpan.FromSeconds(timeTick)).TakeWhile(x => old != cur).TakeUntilDisable(this)
				.Subscribe(_ =>
				{
					old += step;
					if (Mathf.Abs(old - cur) < 0.5f)
						old = cur;
					text.text = old.ToString("0");
				}).AddTo(disAnima);
		}
	}
}
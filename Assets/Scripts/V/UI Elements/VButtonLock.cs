﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System.Linq;
using UnityEngine.UI;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;

#endif

public class VButtonLock : VButton
{
	[Space(20)] [Header("Закрытая кнопка")] [Header("Объекты")]
	public Image lockImage;

	public GameObject coinObj;
	public Text costText;

	[Header("Настройки")] public TypeButtonLock typeButton;
	public int typeNum;

	[Header("ID2")] public string idText;

	[Header("Параметры")] [DisableInEditorMode]
	public int cost;

	[DisableInEditorMode]
	public int chooseNum;

	[DisableInEditorMode]
	public IntReactiveProperty numLock;

	public static Subject<VButtonLock> onClickLockButton = new Subject<VButtonLock>();
	public static Subject<string> buyEvent = new Subject<string>();
	
	public enum TypeButtonLock
	{
		openOne,
		openAllChoose,
		openConstantly
	}

	void OnEnable()
	{
		SetOnEnable();

		var setting = Setting.Inst;
		chooseNum = typeButton == TypeButtonLock.openOne ? setting.choose : 0;
		cost = idText == "" ? setting.GetCostItem(chooseNum, typeNum, id) : setting.GetCostItem(idText);

		numLock.TakeUntilDisable(this).Subscribe(x => SetView());

		if (idText != "")
			numLock.Value = cost == 0 || setting.GetOpenItem(idText) ? 0 : (cost > 0 ? 2 : 1);
		else
			numLock.Value = cost == 0 || setting.GetOpenItem(chooseNum, typeNum, id) ? 0 : (cost > 0 ? 2 : 1);


		onClick.TakeUntilDisable(this).Where(x => x.nameObject == nameObject && x.id == id).Subscribe(__ =>
		{
			if (cost == 0 ||
			    (idText == "" ? setting.GetOpenItem(chooseNum, typeNum, id) : setting.GetOpenItem(idText))) {
				onClickLockButton.OnNext(this);
				return;
			}

			if (cost == -1) {
				VAdsPopup.Init();
				VAdsPopup.eventOk.Take(1).Subscribe(_ =>
				{
					if (typeButton != TypeButtonLock.openConstantly) {
						if (idText == "")
							setting.OnLockSaveItem(chooseNum, typeNum, id);
						else
							setting.OnLockSaveItem(idText);
						numLock.Value = 0;
					}
					else {
						onClickLockButton.OnNext(this);
					}
				}).AddTo(VAdsPopup.disPopup);
			}
			else {
				if (setting.coin.Value >= cost) {
					VAdsPopup.Init(2, 0, cost);
					VAdsPopup.eventOk.Take(1).Subscribe(_ =>
					{
						setting.coin.Value -= cost;
						if (idText == "")
							setting.OnLockSaveItem(chooseNum, typeNum, id);
						else
							setting.OnLockSaveItem(idText);
						numLock.Value = 0;
						buyEvent.OnNext(nameObject);
					}).AddTo(VAdsPopup.disPopup);
				}
				else {
					VAdsPopup.Init(1, setting.db.coinForAds, cost, setting.coin.Value);
					VAdsPopup.eventOk.Take(1).Subscribe(_ => { setting.coin.Value += setting.db.coinForAds; })
						.AddTo(VAdsPopup.disPopup);
				}
			}
		});
	}

	public void SetView()
	{
		if (typeButton != TypeButtonLock.openConstantly) {
			lockImage.gameObject.SetActive(numLock.Value == 1);
			coinObj.gameObject.SetActive(numLock.Value == 2);
			costText.text = cost.ToString();
		}
	}

#if UNITY_EDITOR
	[ContextMenu("Задать индексы")]
	void setID()
	{
		var list = Selection.gameObjects.ToList();
		list = list.OrderBy(x => x.name).ToList();

		for (int i = 0; i < list.Count; i++) {
			var vbl = list[i].GetComponent<VButtonLock>();
			if (vbl != null)
				vbl.id = i;
		}
	}
#endif

	public static void AsObservable(string name, Action<VButtonLock> action)
	{
		onClickLockButton.Where(x => x.nameObject == name).Subscribe(action);
	}
}
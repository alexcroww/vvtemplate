﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class VButtonSwitch : MonoBehaviour
{
	public ButtonType type;
	public List<Sprite> spriteList;

	Button btn;
	CompositeDisposable dis;

	public enum ButtonType
	{
		sound,
		camera,
		vibro,
		light,
		music
	}

	void OnEnable()
	{
		dis = new CompositeDisposable();
		btn = GetComponent<Button>();

		if (type == ButtonType.sound) {
			rxBtn(VPreferences.soundEnabled);
		}
		else if (type == ButtonType.camera) {
			rxBtn(VPreferences.cameraEnabled);
		}
		else if (type == ButtonType.vibro) {
			rxBtn(VPreferences.vibrationEnabled);
		}
		else if (type == ButtonType.light) {
			rxBtn(VPreferences.lightEnabled);
		}
		else if (type == ButtonType.music) {
			rxBtn(VPreferences.musicEnabled);
		}
	}

	void rxBtn(BoolReactiveProperty typeEnable)
	{
		btn.OnClickAsObservable().Subscribe(_ =>
		{

			typeEnable.Value = !typeEnable.Value;
			if (typeEnable.Value)
				VSound.Click();
			else
				VSound.Play("Back");
		}).AddTo(dis);
		typeEnable.Subscribe(b =>
		{
			if (spriteList.Count > 1)
				btn.image.sprite = spriteList[b ? 1 : 0];
			else
				btn.image.color = new Color(b ? 1f : 0.5f, b ? 1f : 0.5f, b ? 1f : 0.5f);
		}).AddTo(dis);
	}

	void OnDisable()
	{
		dis.Clear();
	}
}

﻿using System.Linq;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class VSlider : MonoBehaviour
{
	public string nameSlider;
	public Slider slider;
	public FloatReactiveProperty sliderValue;

	void Awake()
	{
		if (slider == null)
			GetComponent<Slider>();
		sliderValue.Value = PlayerPrefs.GetFloat(nameSlider, sliderValue.Value);
		slider.value = sliderValue.Value;
	}

	void OnEnable()
	{
		slider.ObserveEveryValueChanged(s => s.value).TakeUntilDisable(this).Subscribe(v =>
		{
			PlayerPrefs.SetFloat(nameSlider, v);
			sliderValue.Value = v;
		}).AddTo(this);
	}

	public static VSlider GetSlider(string nameObject)
	{
		var list = Resources.FindObjectsOfTypeAll<VSlider>();
		var rtn = list.FirstOrDefault(x => x.nameSlider == nameObject && x.gameObject.scene.name != null);
		if (rtn == null)
			Debug.LogError("Слайдера с именем <b>" + nameObject + ">/b> не существует");

		return rtn;
	}
}
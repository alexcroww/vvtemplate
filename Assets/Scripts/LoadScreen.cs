﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using DG.Tweening;

public class LoadScreen : MonoBehaviour
{
	static LoadScreen inst;

	[Header("UI")] public Image loadProgressImage;
	AsyncOperation asyncLoadScene;
	[Header("Параметры")] public string sceneLoadName;
	public float timeLoading;
	public bool isCanLoad = false;

	public Action endLoadingAction;

	public static LoadScreen Inst {
		get {
			if (!inst) {
				inst = Resources.FindObjectsOfTypeAll<LoadScreen>()
					.FirstOrDefault(v => v.gameObject.scene.name != null);
			}

			return inst;
		}
	}

	public void Load(string scene = null, float timeLoad = 1f)
	{
		sceneLoadName = scene;
		timeLoading = timeLoad;
		gameObject.SetActive(true);
		Show();
	}

	void Show()
	{
		//Заполнение полосы загрузки
		loadProgressImage.fillAmount = 0f;
		loadProgressImage.DOFillAmount(1f, timeLoading).SetEase(Ease.Linear).OnComplete(() => { isCanLoad = true; });

		isCanLoad = false;

		if (!string.IsNullOrEmpty(sceneLoadName))
			StartCoroutine(AsyncLoadScene());
		else {
			endLoadingAction = () => { gameObject.SetActive(false); };
			DOVirtual.DelayedCall(timeLoading, () => { endLoadingAction.Invoke(); }, false);
		}
	}

	IEnumerator AsyncLoadScene()
	{
		yield return null;

		AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneLoadName);
		asyncOperation.allowSceneActivation = false;

		while (!asyncOperation.isDone) {
			if (asyncOperation.progress >= 0.9f) {
				asyncOperation.allowSceneActivation = isCanLoad;
			}

			yield return null;
		}
	}
}
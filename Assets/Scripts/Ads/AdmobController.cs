﻿#if !DISABLE_ADMOB
using GoogleMobileAds.Api;
using System.Collections;
using System;
#endif
using UnityEngine;

namespace Ads
{
	public enum ShowResult
	{
		Failed,
		Skipped,
		Finished,
	}

	public class AdmobController : MonoBehaviour
	{
		[SerializeField] string androidBanner;
		[SerializeField] string androidInterstitial;
		[SerializeField] string androidReward;
		[SerializeField] string iosBanner;
		[SerializeField] string iosInterstitial;
		[SerializeField] string iosReward;
#if !DISABLE_ADMOB
		InterstitialAd interstitial;
		BannerView bannerView;
		RewardBasedVideoAd rewardBasedVideo;
#endif
		string bannerId;
		string interId;
		string rewardId;
		static bool inited;
		int loadingsCount;
		public float timeoutAds = 20f;
		public static AdmobController instance { get; private set; }

		public static float timerAds;

		public static bool isRewardLoaded {
			get { return instance.rewardBasedVideo.IsLoaded(); }
		}

		public static void SceneLoading()
		{
			if (instance == null) return;

//            if (instance.loadingsCount++ % 2 == 1) instance.Show(0);
			if (timerAds <= 0f)
				instance.Show(0.5f);
		}

		void Awake()
		{
			if (instance == null) {
				DontDestroyOnLoad(gameObject);
				instance = this;
			}
			else DestroyImmediate(gameObject);
		}

		void Update()
		{
			timerAds -= Time.deltaTime;
		}

#if !DISABLE_ADMOB
		void Start()
		{
			Init();
		}

		void Init()
		{
			if (!inited) {
#if UNITY_ANDROID
				bannerId = androidBanner;
				interId = androidInterstitial;
				rewardId = androidReward;
#elif UNITY_IOS
				bannerId = iosBanner;
			   interId = iosInterstitial;
				rewardId = iosReward;
#endif
				inited = true;
				RequestBanner();
				RequestInterstitial();

				// Get singleton reward based video ad reference.
				rewardBasedVideo = RewardBasedVideoAd.Instance;
				// Called when an ad request has successfully loaded.
				rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
				// Called when an ad request failed to load.
				rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
				// Called when an ad is shown.
				rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
				// Called when the ad starts to play.
				rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
				// Called when the user should be rewarded for watching a video.
				rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
				// Called when the ad is closed.
				rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
				// Called when the ad click caused the user to leave the application.
				rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

				RequestRewardBasedVideo();
			}
			else RemoveAds();
		}

		void HandleRewardBasedVideoLeftApplication(object sender, EventArgs e)
		{
			print("HandleRewardBasedVideoLeftApplication event received");
		}

		void HandleRewardBasedVideoClosed(object sender, EventArgs e)
		{
			if (callback != null) {
				callback(ShowResult.Skipped);
				RequestRewardBasedVideo();
			}
		}

		void HandleRewardBasedVideoRewarded(object sender, Reward e)
		{
			if (callback != null) {
				callback(ShowResult.Finished);
				RequestRewardBasedVideo();
			}
		}

		void HandleRewardBasedVideoStarted(object sender, EventArgs e)
		{
			print("HandleRewardBasedVideoStarted event received");
		}

		void HandleRewardBasedVideoOpened(object sender, EventArgs e)
		{
			print("HandleRewardBasedVideoOpened event received");
		}

		void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs e)
		{
//			isRewardLoaded = false;
			if (callback != null) {
				callback(ShowResult.Failed);
//				print("VideoFailedToLoad message: " + e.Message);
				RequestRewardBasedVideo();
			}
		}

		public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
		{
			print("VideoLoaded");
//			throw new NotImplementedException();
		}

		private void RequestRewardBasedVideo()
		{
			// Load the rewarded video ad with the request.
			rewardBasedVideo.LoadAd(adRequest, rewardId);
		}

		Action<ShowResult> callback;

		public static void ShowReward(Action<ShowResult> callback)
		{
			instance.callback = callback;
			instance.rewardBasedVideo.Show();
		}

		void RemoveAds()
		{
			if (bannerView != null) bannerView.Hide();
		}

		void RequestBanner()
		{
			bannerView = new BannerView(bannerId, AdSize.SmartBanner, AdPosition.Bottom);
			bannerView.LoadAd(adRequest);
		}

		static AdRequest adRequest {
			get {
				return new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator) // Simulator
					.AddTestDevice("A0AB3BAEACD14BE788D16BBD4844AC62") // Alexandr Android
					.Build();
			}
		}

		void RequestInterstitial()
		{
			interstitial = new InterstitialAd(interId);
			interstitial.LoadAd(adRequest);
			interstitial.OnAdClosed += HandleAdClosed;
		}

		void HandleAdClosed(object _, EventArgs __)
		{
			interstitial.OnAdClosed -= HandleAdClosed;
			RequestInterstitial();
		}

		void Show()
		{
			if (interstitial != null && interstitial.IsLoaded() == true) {
				interstitial.Show();
			}
		}

		void Show(float time)
		{
			if (time > 0) StartCoroutine(WaitAndShowAd(time));
			else Show();
			timerAds = timeoutAds;
			LoadScreen.Inst.Load();
		}

		IEnumerator WaitAndShowAd(float waitTime)
		{
			yield return new WaitForSeconds(waitTime);
			Show();
		}

		/*void OnDestroy()
		{
			rewardBasedVideo.OnAdLoaded -= HandleRewardBasedVideoLoaded;
			rewardBasedVideo.OnAdFailedToLoad -= HandleRewardBasedVideoFailedToLoad;
			rewardBasedVideo.OnAdOpening -= HandleRewardBasedVideoOpened;
			rewardBasedVideo.OnAdStarted -= HandleRewardBasedVideoStarted;
			rewardBasedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
			rewardBasedVideo.OnAdClosed -= HandleRewardBasedVideoClosed;
			rewardBasedVideo.OnAdLeavingApplication -= HandleRewardBasedVideoLeftApplication;
		}*/
#endif
	}
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
using Ads;

public class StartScreen : MonoBehaviour
{
//	[Header("Настройки")] 

	[Header("Экраны")] public List<CanvasGroup> screenCanvasGroup;
	public RectTransform mainCanvasRect;

	[Header("Инфо")] public GameObject infoPrefab;

	[Header("Загрузки")] public bool isFakeLoading;

	void Awake()
	{
		Setting.Inst.Init();

		rxSubscribe();

		Time.timeScale = 1f;

		if (!isFakeLoading) {
			LoadScreen.Inst.Load(null, Stats.Inst.stats.isTestMode?0.5f:5f);
		}

		Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
		{
			var dependencyStatus = task.Result;
			if (dependencyStatus == Firebase.DependencyStatus.Available) {
				// Create and hold a reference to your FirebaseApp,
				// where app is a Firebase.FirebaseApp property of your application class.
				//   app = Firebase.FirebaseApp.DefaultInstance;

				// Set a flag here to indicate whether Firebase is ready to use by your app.
			}
			else {
				UnityEngine.Debug.LogError(System.String.Format(
					"Could not resolve all Firebase dependencies: {0}", dependencyStatus));
				// Firebase Unity SDK is not safe to use here.
			}
		});
	}

	void rxSubscribe()
	{
		var cd = new CompositeDisposable();
		if (GameObject.Find("cheatBtn")) {
			var cheatBtn = GameObject.Find("cheatBtn").GetComponent<Image>();
			cheatBtn.OnPointerDownAsObservable().Delay(TimeSpan.FromSeconds(5f))
				.Subscribe(__ =>
				{
					Setting.Inst.OnLockAllItem();
					VSound.Click();
				}).AddTo(cd);
			cheatBtn.OnPointerUpAsObservable().Subscribe(_ => cd.Clear()).AddTo(cd);
		}

		//Кнопки
		VButton.onClick.Where(x => x.nameObject == "play").Subscribe(_ =>
		{
			ChangeScreen(screenCanvasGroup[1], screenCanvasGroup[0], 2);
		}).AddTo(this);

		VButton.onClick.Where(x => x.nameObject == "more").Subscribe(_ =>
#if UNITY_ANDROID
				Application.OpenURL("market://search?q=pub:" + Setting.Inst.ab.GetAccounts().value[1])
#elif UNITY_IOS
				Application.OpenURL(Setting.Inst.ab.GetAccounts().value[1])
#elif UNITY_EDITOR
			Debug.Log("More Games: "+ Setting.Inst.ab.GetAccounts().value[1])
#endif
		).AddTo(this);
		VButton.onClick.Where(x => x.nameObject == "policy").Subscribe(_ =>
		{
			Application.OpenURL(Setting.Inst.ab.GetAccounts().value[4]);
		}).AddTo(this);
		VButton.onClick.Where(x => x.nameObject == "back_choose").Subscribe(_ =>
		{
			ChangeScreen(screenCanvasGroup[0], screenCanvasGroup[1], 3);
		}).AddTo(this);
		VButton.onClick.Where(x => x.nameObject == "back_play").Subscribe(_ =>
		{
			ChangeScreen(screenCanvasGroup[1], screenCanvasGroup[2], 3);
		}).AddTo(this);
		VButton.onClick.Where(x => x.nameObject == "choose").Subscribe(_ =>
		{
			ChangeScreen(screenCanvasGroup[2], screenCanvasGroup[1], 3);
		}).AddTo(this);
		VButtonLock.onClickLockButton.Where(x => x.nameObject == "choose").Subscribe(x =>
		{
			Setting.Inst.choose = x.id;
			ChangeScreen(screenCanvasGroup[2], screenCanvasGroup[1], 3);
		}).AddTo(this);

		VButton.onClick.Where(x => x.nameObject == "info").Subscribe(_ =>
		{
			var obj = Instantiate(infoPrefab, Vector3.zero, Quaternion.identity, mainCanvasRect);
			obj.transform.localScale = Vector3.one;
			obj.transform.localPosition = Vector3.zero;
			obj.name = "Info";
			obj.SetActive(true);
		}).AddTo(this);

		VButton.onClick.Where(x => x.nameObject == "info_close").Subscribe(_ =>
		{
			var obj = VLab.FindChildObj(mainCanvasRect, "Info");
			Destroy(obj);
		}).AddTo(this);

		VPreferences.sleepEnabled.Subscribe(b =>
		{
			Screen.sleepTimeout = b ? SleepTimeout.SystemSetting : SleepTimeout.NeverSleep;
		}).AddTo(this);
	}

	public void ChangeScreen(CanvasGroup show, CanvasGroup hide, int mode = 0)
	{
		AdmobController.SceneLoading();
		const float timeA = 0.3f;
		hide.interactable = false;
		show.interactable = false;
		show.alpha = 1f;
		var isUp = false;

		switch (mode) {
			case 0:
				hide.gameObject.SetActive(false);
				show.gameObject.SetActive(true);
				hide.interactable = true;
				show.interactable = true;
				break;
			case 1:
				show.alpha = 0f;
				show.gameObject.SetActive(true);
				show.DOFade(1f, timeA);
				hide.DOFade(0f, timeA);
				Observable.Timer(TimeSpan.FromSeconds(timeA)).Subscribe(_ =>
				{
					hide.gameObject.SetActive(false);
					hide.interactable = true;
					show.interactable = true;
				}).AddTo(this);
				break;
			case 3:
				isUp = screenCanvasGroup.FindIndex(x => x == show) > screenCanvasGroup.FindIndex(x => x == hide);
				show.transform.localPosition = new Vector3(mainCanvasRect.rect.width * (isUp ? 1 : -1), 0f);
				show.gameObject.SetActive(true);
				if (isUp) {
					show.transform.DOLocalMoveX(0f, timeA);
				}
				else {
					show.transform.DOLocalMoveX(0f, 0f);
					hide.transform.DOLocalMoveX(-mainCanvasRect.rect.width * (-1), timeA);
				}

				Observable.Timer(TimeSpan.FromSeconds(timeA)).Subscribe(_ =>
				{
					hide.gameObject.SetActive(false);
					hide.transform.localPosition = Vector3.zero;
					hide.interactable = true;
					show.interactable = true;
				}).AddTo(this);
				break;
			case 2:
				isUp = screenCanvasGroup.FindIndex(x => x == show) > screenCanvasGroup.FindIndex(x => x == hide);
				show.transform.localPosition = new Vector3(mainCanvasRect.rect.width * (isUp ? 1 : -1), 0f);
				show.gameObject.SetActive(true);
				show.transform.DOLocalMoveX(0f, timeA);
				hide.transform.DOLocalMoveX(-mainCanvasRect.rect.width * (isUp ? 1 : -1), timeA);
				Observable.Timer(TimeSpan.FromSeconds(timeA)).Subscribe(_ =>
				{
					hide.gameObject.SetActive(false);
					hide.transform.localPosition = Vector3.zero;
					hide.interactable = true;
					show.interactable = true;
				}).AddTo(this);
				break;
		}

		if (show == screenCanvasGroup[0]) {
			VUIAnimator.SetDefault("start");
			Observable.Timer(System.TimeSpan.FromSeconds(timeA)).Subscribe(_ => { VUIAnimator.SetState("start", 1); });
		}

		if (show == screenCanvasGroup[1]) {
			VUIAnimator.SetDefault("choose");
			Observable.Timer(System.TimeSpan.FromSeconds(timeA)).Subscribe(_ => { VUIAnimator.SetState("choose", 1); });
		}
	}

	public void btnShowAds()
	{
		VSound.Click();
		VAdsPopup.Init(0, Setting.Inst.db.coinForAds);
		VAdsPopup.eventOk.Take(1).Subscribe(_ => { Setting.Inst.coin.Value += Setting.Inst.db.coinForAds; })
			.AddTo(VAdsPopup.disPopup);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (screenCanvasGroup[0].gameObject.activeSelf)
				Application.Quit();
			if (!screenCanvasGroup[0].gameObject.activeSelf && screenCanvasGroup[1].gameObject.activeSelf &&
			    screenCanvasGroup[1].interactable) {
				VSound.Click();
				ChangeScreen(screenCanvasGroup[0], screenCanvasGroup[1], 3);
			}

			if (!screenCanvasGroup[1].gameObject.activeSelf && screenCanvasGroup[2].gameObject.activeSelf &&
			    screenCanvasGroup[2].interactable) {
				VSound.Click();
				ChangeScreen(screenCanvasGroup[1], screenCanvasGroup[2], 3);
			}
		}
	}

	[ContextMenu("Очистить Prefs")]
	void deletePrefs()
	{
		PlayerPrefs.DeleteAll();
	}
}
﻿using UnityEngine;
using UniRx;

public class ChooseScreen : MonoBehaviour
{
	void OnEnable()
	{
		VToogle.GetToggle("sound").isOn
			.DoOnSubscribe(() => { VToogle.GetToggle("sound").isOn.Value = VPreferences.soundEnabled.Value; })
			.TakeUntilDisable(this).Subscribe(b => { VPreferences.soundEnabled.Value = b; }).AddTo(this);

		VToogle.GetToggle("sleep").isOn
			.DoOnSubscribe(() => { VToogle.GetToggle("sleep").isOn.Value = VPreferences.sleepEnabled.Value; })
			.TakeUntilDisable(this).Subscribe(b => { VPreferences.sleepEnabled.Value = b; }).AddTo(this);

		VSlider.GetSlider("speed").sliderValue
			.TakeUntilDisable(this).Subscribe(v => { Setting.Inst.speed.Value = v; }).AddTo(this);
		VSlider.GetSlider("size").sliderValue
			.TakeUntilDisable(this).Subscribe(v => { Setting.Inst.size.Value = v; }).AddTo(this);
	}
}
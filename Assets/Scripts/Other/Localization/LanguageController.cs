﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LanguageController
{
    
	public class Trans
	{
		public string code;
		public string[] val;

		public Trans(string code, string[] val)
		{
			this.code = code;
			this.val = val;
		}
	};

	public static string GetString(string s)
	{
		for (int i = 0; i < translations.Length; i++) {
			if (translations[i].code == s) {
				if (Application.systemLanguage == SystemLanguage.Russian) {
					return translations[i].val[0];
				} else {
					return translations[i].val[1];
				}
			}
		}
		return s;
	}

	public static Trans[] translations = new Trans[] {
		new Trans("unity_failed", new string[] {
			"Не удалось загрузить видео. Проверьте соединение с интернетом",
			"Failed to load the video. Check internet connection"
		}),
		new Trans("error", new string[] {"Ошибка","Error"}),
		new Trans("record_failed", new string[] { "Не удалось определить голос", "Unable to determine the voice" }),
		new Trans("save_image", new string[] { "Изображение сохранено", "Image saved" }),
		new Trans("video", new string[] { "Открыть новый элемент за просмотр рекламы?", "Open new element for viewing ads?" }),
		new Trans("yes", new string[] { "Да", "Yes" }),
		new Trans("saved", new string[]{ "Сохранено", "Saved" }),
		new Trans("iplay", new string[] { "Я узнал свой психологический возраст!", "I found my psychological age!" }),
		new Trans("inapp", new string[] { "в приложении", "in the app" }),
		new Trans("app_name", new string[] { "Название приложения на русском", "App Name" })
	};
		
}
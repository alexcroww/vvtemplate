﻿using UnityEngine;
using UnityEngine.UI;

public class LocalizeText : MonoBehaviour
{
	public string text_ru;

	void Awake()
	{
		if (Application.systemLanguage == SystemLanguage.Russian)
			GetComponent<Text>().text = text_ru.Replace("[enter]", "\n");
	}
}

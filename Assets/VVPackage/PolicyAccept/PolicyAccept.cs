﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Соглашение о лицензии
/// </summary>
public class PolicyAccept : MonoBehaviour
{
	//Указываем объект/канвас на котором будет появляться запрос соглашения лицензии
	public Transform path;
	public GameObject policyPrefab;
	Button yesBtn;
	Toggle toggleAccept;
	float timeScaleTemp;

	void Start()
	{
		if (PlayerPrefs.HasKey("policy_accept")) {
			Destroy(this);
		}
		else {
			if (path == null)
				path = transform;

			timeScaleTemp = Time.timeScale;
			Time.timeScale = 0f;

			var obj = Instantiate(policyPrefab, Vector3.zero, Quaternion.identity, path);
			obj.transform.localScale = Vector3.one;
			obj.transform.localPosition = Vector3.zero;
			obj.SetActive(true);
			
			yesBtn = obj.GetComponentInChildren<Button>();
			yesBtn.gameObject.SetActive(false);
			toggleAccept = obj.GetComponentInChildren<Toggle>();
			toggleAccept.isOn = false;

			toggleAccept.onValueChanged.AddListener(yesBtn.gameObject.SetActive);

			yesBtn.onClick.AddListener(() =>
			{
				PlayerPrefs.SetInt("policy_accept", 1);
				Time.timeScale = timeScaleTemp;
				Destroy(obj);
				Destroy(this);
			});
		}
	}
}